mavenJob('dsljob01') {
  description('auto generated job created by xbalaji')
  logRotator {
    numToKeep(2)
  }
  scm {
    git {
      remote {
        url('https://xbalaji@bitbucket.org/xbalaji/mvn02.git')
      }
      branch('master')
    }
  }

  rootPOM('xbmvn02-app/pom.xml')
  goals('package cobertura:cobertura pmd:pmd findbugs:findbugs')

  postBuildSteps('SUCCESS') {
    configure {
      it / 'postbuilders' / 'hudson.plugins.sonar.SonarRunnerBuilder' {
        properties(
            "sonar.projectKey=xb:dslmvn01\n" +
            "sonar.host.url=http://192.168.1.54:9000\n" +
            "sonar.projectName=dslmvn01\n" +
            "sonar.projectVersion=1.0\n" +
            "sonar.sources=.\n" +
            "sonar.cobertura.reportPath=xbmvn02-app/target/site/cobertura/coverage.xml\n" +
            "sonar.surefire.reportsPath=xbmvn02-app/target/test-reports\n"
        )
      }
    }
    publishers {
      findbugs('xbmvn02-app/target/findbugsXml.xml', false)
      archiveJunit('xbmvn02-app/target/surefire-reports/*.xml')
      pmd('xbmvn02-app/target/pmd.xml')
      cobertura('xbmvn02-app/target/site/cobertura/coverage.xml')
    }
  }
}
