import groovy.io.FileType
import groovy.json.JsonSlurper


MY_ORG_FOLDER='MyOrg'

PROJ_SPEC_FOLDER="${WORKSPACE}/cfg"
println "${PROJ_SPEC_FOLDER}"

static String conv2camel (String text) {
    return text.replaceAll( "(_)([A-Za-z0-9])", { Object[] it -> it[2].toUpperCase() } )
}


def jsonCfgDir = new File("${PROJ_SPEC_FOLDER}")
jsonCfgDir.eachFile() {
  file -> if (file.name.endsWith('.json')) {
    println "$file.name"

    def jsonContent = new JsonSlurper().parseText(readFileFromWorkspace("$file.absolutePath"))
    def name = jsonContent."name"
    def type = jsonContent."type"
    def project = jsonContent."project"
    def branch = jsonContent."branch"

    def isclean  = jsonContent."prebuildclean"
    def findbugs = jsonContent."findbugs"
    def junit    = jsonContent."junit"
    def pmd      = jsonContent."pmd"
    def cobertura= jsonContent."cobertura"
    def sonar    = jsonContent."sonar"
    def sonarurl = jsonContent."sonarurl"
    def combinedsa = jsonContent."combinedsa"

    def sonarprojkey = "${project}.${branch}".toLowerCase()
    sonarprojkey = sonarprojkey.replaceAll('/','-')

    def sonarprojname = "_${project}_${branch}".toLowerCase()
    sonarprojname = conv2camel("${sonarprojname}")

    println "name: $name"
    println "type: $type"
    println "project: $project"
    println "branch: $branch"
    println "isclean: $isclean"
    println "findbugs: $findbugs"
    println "junit: $junit"
    println "pmd: $pmd"
    println "cobertura: $cobertura"
    println "sonarurl: $sonarurl"
    println "sonarprojkey: $sonarprojkey"
    println "sonarprojname: $sonarprojname"

    job("${MY_ORG_FOLDER}/${project}-${branch}") {
      description("auto generated job for building ${project}-${branch}")
      logRotator {
        numToKeep(2)
      }

      /* add scm stuff here */
      scm {
        git {
          remote {
            url("https://xbalaji@bitbucket.org/xbalaji/${project}.git")
          }
        }
      }

      if (isclean) {
        wrappers {
          preBuildCleanup()
        }
      }

      triggers {
        scm('H/5 * * * *')
      }

      steps {
        /* shell('echo works') */
        if ("$type" == "maven") {
          maven {
            mavenInstallation('maven_3_3_9')
            goals('package cobertura:cobertura pmd:pmd findbugs:findbugs')
          }
        }
        if ("$sonar") {
          configure {
            installationName("sonar_on_docker")
            it / 'builders' / 'hudson.plugins.sonar.SonarRunnerBuilder' {
              properties("sonar.host.url=${sonarurl}\n" +
                "sonar.projectKey=${sonarprojkey}\n"    +
                "sonar.projectName=${sonarprojname}\n"  +
                "sonar.projectVersion=1.0\n"            +
                "sonar.sources=.\n"                     +
                "sonar.cobertura.reportPath=target/site/cobertura/coverage.xml\n" +
                "sonar.surefire.reportsPath=target/test-reports\n")
            }
          }
        }
      }


      if ("$junit") {
        publishers {
          archiveJunit("target/surefire-reports/*.xml")
        }
      }

      if ("$pmd") {
        configure {
          it / 'publishers' / 'hudson.plugins.pmd.PmdPublisher' {
            pattern 'target/pmd.xml'
          }
        }
      }

      if ("$findbugs") {
        configure {
          it / 'publishers' / 'hudson.plugins.findbugs.FindBugsPublisher' {
            pattern 'target/findbugsXml.xml'
          }
        }
      }

      if ("$combinedsa") {
        configure {
          it / 'publishers' / 'hudson.plugins.analysis.collector.AnalysisPublisher' {
            isFindBugsDeactivated(false)
            isPmdDeactivated(false)
          }
        }
      }

      if ("$cobertura") {
        configure {
          it / 'publishers' / 'hudson.plugins.cobertura.CoberturaPublisher' {
            coberturaReportFile 'target/site/cobertura/coverage.xml'
            failNoReports(false)
            sourceEncoding('ASCII')
            onlyStable(false)
            failUnhealthy(false)
            failUnstable(false)
            'healthyTarget' {
              targets(class: "enum-map", 'enum-type': "hudson.plugins.cobertura.targets.CoverageMetric") {
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'METHOD'
                  'int' 8000000
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'LINE'
                  'int' 8000000
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'CONDITIONAL'
                  'int' 7000000
                }
              }
            }
            'unhealthyTarget' {
              targets(class: "enum-map", 'enum-type': "hudson.plugins.cobertura.targets.CoverageMetric") {
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'METHOD'
                  'int' 0
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'LINE'
                  'int' 0
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'CONDITIONAL'
                  'int' 0
                }
              }
            }
            'failingTarget' {
              targets(class: "enum-map", 'enum-type': "hudson.plugins.cobertura.targets.CoverageMetric") {
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'METHOD'
                  'int' 0
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'LINE'
                  'int' 0
                }
                entry {
                  'hudson.plugins.cobertura.targets.CoverageMetric' 'CONDITIONAL'
                  'int' 0
                }
              }
            }
          }
        }
      }

    } /* job closure */
  } /* for json file */
}



