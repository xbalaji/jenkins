
MY_ORG_FOLDER='MyOrg'
SEED_JOB='SeedJob'


folder("${MY_ORG_FOLDER}")

job("${MY_ORG_FOLDER}/${SEED_JOB}") {
  description('job to create seed jobs - xbalaji')
  logRotator {
    numToKeep(2)
  }
  scm {
    git {
      remote {
        url('https://bitbucket.org/xbalaji/jenkins.git')
      }
    }
  }
  wrappers {
    preBuildCleanup()
  }
  triggers {
    scm('H/5 * * * *')
  }
  steps {
    /* shell('pwd;echo executing dsl jobs!;env')  */
    /* shell('cp -R /var/jenkins_home/workspace/seed-job-02 .') */
    dsl {
      external("scripts/mvn03.groovy")
    }
  }
}
