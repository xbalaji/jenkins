Create the **masteSeed** job using **jenkins post** 

1. Download 'masterSeed.xml' to your folder
1. Find the crumb for the installed jenkins server
1. Execute the post command to create the 'master seed job'
1. Execute the 'master seed job' to create 'seed jobs'

```
#!txt



Assuming:
(a) jenkins server is installed as: xbjenkins-server
(b) login:xbuser password:xbpass
(c) saved master seed xml as 'masterSeed.xml'


Execute the following
```


```
#!shell

JENK_USER='xbuser'
JENK_PASS='xbpass'
JENK_SERVER='jenkins-server'


export JENK_CRUMB=$(curl -s "http://${JENK_USER}:${JENK_PASS}@${JENK_SERVER}:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")
curl -s -XPOST "http://${JENK_USER}:${JENK_PASS}@${JENK_SERVER}:8080/createItem?name=MasterSeed" --data-binary @masterSeed.xml -H "Content-Type:text/xml" -H "${JENK_CRUMB}"

```

Now login to the jenkins server, find the 'MasterSeed' job, execute it!