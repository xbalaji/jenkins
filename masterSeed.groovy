job('masterSeed') {
  description('job to create seed jobs - xbalaji')
  logRotator {
    numToKeep(1)
  }
  scm {
    git {
      remote {
        url('https://bitbucket.org/xbalaji/jenkins.git')
      }
    }
  }
  wrappers {
    preBuildCleanup()
  }
  triggers {
    scm('H/5 * * * *')
  }
  steps {
    dsl {
      external("seedCreator.groovy")
    }
  }
}
